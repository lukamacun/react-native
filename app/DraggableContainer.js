import React, { Component } from 'react';
import { View, PanResponder, Animated, Dimensions } from 'react-native';

let window = Dimensions.get('window');

let BoxDimensions = {
  width: 150,
  height: 100,
  backgroundColor: 'black'
}

export class DraggableContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pan: new Animated.ValueXY()
        }
        this.PanResponder = PanResponder.create({
            onStartShouldSetPanResponder : () => true,
            onPanResponderMove           : Animated.event([null, {
                dx: this.state.pan.x,
                dy: this.state.pan.y
            }]),
            onPanResponderRelease        : (e, gesture) => {}
        });
    }

    render() {
        return (
            <View>
            {this.renderDraggable()}
            </View>
        )
    }

    renderDraggable() {
        return (
            <Animated.View {...this.PanResponder.panHandlers} style={[this.state.pan.getLayout(),this.props.object.styles()]} />
        )
    }
}
