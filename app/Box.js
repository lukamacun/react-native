import React, { Component } from 'react';
import { View } from 'react-native';

let BoxDimensions = {
  width: 150,
  height: 100,
  backgroundColor: 'black'
}

export class Box extends Component {
  styles = () => {
    return BoxDimensions;
  }
  render() {
    return (
      <View style={BoxDimensions} />
    )
  }
}
