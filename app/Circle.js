import React, { Component } from 'react';
import { View } from 'react-native';

let CIRCLE_RADIUS = 30;

let BoxDimensions = {
  backgroundColor     : '#1abc9c',
    width               : CIRCLE_RADIUS*2,
    height              : CIRCLE_RADIUS*2,
    borderRadius        : CIRCLE_RADIUS
}

export class Circle extends Component {
  styles = () => {
    return BoxDimensions;
  }
  render() {
    return (
      <View style={BoxDimensions} />
    )
  }
}
