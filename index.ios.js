import React, { Component } from 'react';
import { AppRegistry, View } from 'react-native';
import { Box } from './app/Box.js';
import { Circle } from './app/Circle.js';
import { DraggableContainer } from './app/DraggableContainer.js';


class TestNativeReact extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <DraggableContainer object={new Circle} />
        <DraggableContainer object={new Box} />
      </View>
    )
  }
}

AppRegistry.registerComponent('TestNativeReact', () => TestNativeReact);